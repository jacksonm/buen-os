#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(buen_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use buen_os::{println};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed = 0x11,
}

#[no_mangle]
pub extern "C" fn _start() -> ! {

    buen_os::init();

    x86_64::instructions::interrupts::int3();
    println!("Hello World{}", "!");

    #[cfg(test)]
    test_main();

    loop {}
}


/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    buen_os::test_panic_handler(info)
}
